<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210727162049 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add (Date)';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE data (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, description LONGTEXT NOT NULL, vedio VARCHAR(255) DEFAULT NULL, foto VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updeted_at DATETIME DEFAULT NULL, is_deleted TINYINT(1) NOT NULL, INDEX IDX_ADF3F363A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_error (data_id INT NOT NULL, error_id INT NOT NULL, INDEX IDX_136270EE37F5A13C (data_id), INDEX IDX_136270EE836088D7 (error_id), PRIMARY KEY(data_id, error_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F363A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE data_error ADD CONSTRAINT FK_136270EE37F5A13C FOREIGN KEY (data_id) REFERENCES data (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE data_error ADD CONSTRAINT FK_136270EE836088D7 FOREIGN KEY (error_id) REFERENCES error (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE data_error DROP FOREIGN KEY FK_136270EE37F5A13C');
        $this->addSql('DROP TABLE data');
        $this->addSql('DROP TABLE data_error');
    }
}
